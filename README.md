# About Static Test Stand Data

Each semester, the AES LA team collects static test stand data for different water volumes (i.e. variable volume data) and for identical water volumes (i.e. fixed volume data). This data repository allows for the analysis of water bottle rocket impulse characteristics in preparation for a real, live water bottle rocket launch.

## Methodology

These data were collected using the "Water Rocket Static Test Documentation" file posted on Canvas. This document includes both the LA setup procedure as well as the student launch procedure.

## Description

The data files within the repository follow a standardized naming convention within the following structured tree:

```
ASEN 2804/
├─ Water Rocket Lab/
│  ├─ Static Test Stand Data/
│  │  ├─ 1250mL Bottle/
│  │  │  ├─ Variable Volume/
│  │  ├─ 2000mL Bottle/
│  │  │  ├─ Fixed Volume/
│  │  │  ├─ Variable Volume/
```

Each filename starts with a short description of the test in question, such as ``LA_Test_`` to indicate a test performed by the LA team. Furthermore, each filename contains the water volume used for that respective test using the following convention: ``_WXXXXmL`` where ``XXXX`` is the 3- or 4-digit water volume in milliliters.

### Bottle Size
Starting in Spring 2023, the data repository will contain test data for the standard 2L bottle as well as a smaller 1.25L bottle. This change was implemented in order to explore the effect of a smaller container volume as well as its different nozzle geometry. These different data are separated into two distinct folders, ``1250mL Bottle`` and ``2000mL Bottle``, for the 1.25L and 2L bottles, respectively. Furthermore, each data file name is terminated by a specific suffix, either ``_B1250mL`` or ``_B2000mL``, depending on the bottle size used for that respective test.

### Test Category
There are two sets of test data collected for the 2L bottle: a fixed volume set and a variable volume set:
* Fixed Volume: this data set contains multiple trials of a static test stand launch with the same setup parameters, namely with 1000 mL of water. These files contain the test number in their name using the following convention: ``_TXX_`` where ``XX`` is the 2-digit trial number.
* Variable Volume: this data set contains multiple trials of a static test stand launch with varying water volumes, usually ranging from 400 mL to 1000 mL.
